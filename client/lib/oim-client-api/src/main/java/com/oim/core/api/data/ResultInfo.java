package com.oim.core.api.data;

/**
 * @author XiaHui
 * @date 2017-11-23 09:46:41
 */
public enum ResultInfo {

	success, error, timeout, lost;
}
