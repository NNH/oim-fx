package com.oim.core.api.requester;

import java.util.concurrent.TimeUnit;

import com.oim.core.api.module.NetworkModule;
import com.only.general.blocking.QueueBlockingHandler;
import com.only.net.data.action.DataBackAction;
import com.onlyxiahui.app.base.AbstractComponent;
import com.onlyxiahui.app.base.AppContext;
import com.onlyxiahui.im.message.Data;
import com.onlyxiahui.im.message.Head;
import com.onlyxiahui.im.message.client.ClientHead;

/**
 * @author: XiaHui
 * @date: 2018-03-15 16:02:10
 */
public class BaseRequester extends AbstractComponent {

	QueueBlockingHandler<Object> blocking=new QueueBlockingHandler<>();
	
	private String clientVersion = "0.1";
	private String clientType = "1";

	public BaseRequester(AppContext appContext) {
		super(appContext);
		blocking.setTimeOut(5000,TimeUnit.MILLISECONDS);
	}

	public void write(Data data, DataBackAction dataBackAction) {
		NetworkModule nm = appContext.getModule(NetworkModule.class);
		nm.write(data, dataBackAction);
	}

	public Head getHead() {
		ClientHead head = new ClientHead();
		head.setClientType(this.getClientType());
		head.setClientVersion(this.getClientVersion());
		return head;
	}

	public String getClientVersion() {
		return clientVersion;
	}

	public void setClientVersion(String clientVersion) {
		this.clientVersion = clientVersion;
	}

	public String getClientType() {
		return clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}
}
