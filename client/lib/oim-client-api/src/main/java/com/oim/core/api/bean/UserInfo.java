package com.oim.core.api.bean;

import com.onlyxiahui.im.bean.UserData;
import com.onlyxiahui.im.bean.UserHead;

/**
 * @author: XiaHui
 * @date: 2018-03-15 15:17:58
 */
public class UserInfo {

	private UserData userData;
	private UserHead userHead;

	public UserData getUserData() {
		return userData;
	}

	public void setUserData(UserData userData) {
		this.userData = userData;
	}

	public UserHead getUserHead() {
		return userHead;
	}

	public void setUserHead(UserHead userHead) {
		this.userHead = userHead;
	}
}
