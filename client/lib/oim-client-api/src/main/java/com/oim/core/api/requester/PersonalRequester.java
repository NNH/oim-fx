package com.oim.core.api.requester;

import java.util.concurrent.TimeUnit;

import com.oim.common.util.KeyUtil;
import com.oim.core.api.bean.UserInfo;
import com.oim.core.api.data.ResultData;
import com.oim.core.api.data.ResultStatus;
import com.only.common.result.Info;
import com.only.general.annotation.parameter.Define;
import com.only.general.blocking.QueueBlockingHandler;
import com.only.net.action.Back;
import com.only.net.data.action.DataBackActionAdapter;
import com.onlyxiahui.app.base.AppContext;
import com.onlyxiahui.im.bean.UserData;
import com.onlyxiahui.im.bean.UserHead;
import com.onlyxiahui.im.message.Head;
import com.onlyxiahui.im.message.client.Message;
import com.onlyxiahui.im.message.data.LoginData;

/**
 * @author XiaHui
 * @date 2017-11-20 22:05:34
 */
public class PersonalRequester extends BaseRequester {

	public PersonalRequester(AppContext appContext) {
		super(appContext);
	}

	public ResultData<UserInfo> login(LoginData loginData) {
		final QueueBlockingHandler<Object> blocking=new QueueBlockingHandler<>();
		final String key = KeyUtil.getKey();
		final ResultData<UserInfo> rd = new ResultData<UserInfo>();

		final DataBackActionAdapter action = new DataBackActionAdapter() {// 这是消息发送后回掉
			@Override
			public void lost() {
				rd.addWarning("001", "登录失败，请检查网络是否正常。");
				rd.setStatus(ResultStatus.lost);
			}

			@Override
			public void timeOut() {
				rd.addWarning("001", "登录失败，请检查网络是否正常。");
				rd.setStatus(ResultStatus.lost);
			}

			@Back
			public void back(
					Info info,
					@Define("userData") UserData user,
					@Define("userHead") UserHead userHead) {

				rd.setInfo(info);

				UserInfo ui = new UserInfo();
				ui.setUserData(user);
				ui.setUserHead(userHead);

				blocking.put(key, ui);
			}
		};

		Message message = new Message();
		message.put("loginData", loginData);

		Head head = this.getHead();
		head.setKey(key);
		head.setAction("1.100");
		head.setMethod("1.1.0001");
		head.setTime(System.currentTimeMillis());
		message.setHead(head);
		this.write(message, action);

		UserInfo ui = (UserInfo) blocking.get(key, 5000, TimeUnit.MILLISECONDS);
		rd.set(ui);
		if (null == ui) {
			rd.addWarning("001", "登录失败，请检查网络是否正常。");
			rd.setStatus(ResultStatus.lost);
		}
		return rd;
	}
}
