package com.oim.core.api;

import java.util.concurrent.TimeUnit;

import com.oim.core.api.module.ActionModule;
import com.oim.core.api.module.NetworkModule;
import com.oim.core.api.requester.PersonalRequester;
import com.only.general.blocking.QueueBlockingHandler;
import com.only.net.action.ConnectBackAction;
import com.only.net.connect.ConnectData;
import com.onlyxiahui.app.base.AppContext;

/**
 * @author XiaHui
 * @date 2017-11-20 21:47:20
 */
public class OimContext extends AppContext {

	public OimContext() {
		initContext();
	}

	private void initContext() {
		NetworkModule networkModule = this.getModule(NetworkModule.class);
		networkModule.start();
		initAction();
	}

	public ActionModule getActionModule() {
		ActionModule actionModule = this.getModule(ActionModule.class);
		return actionModule;
	}

	public NetworkModule getNetworkModule() {
		NetworkModule networkModule = this.getModule(NetworkModule.class);
		return networkModule;
	}

	private void initAction() {

	}

	public boolean connect(ConnectData connectData) {
		final String key = "connect";
		final QueueBlockingHandler<Boolean> blocking = new QueueBlockingHandler<>();

		ConnectBackAction cba = new ConnectBackAction() {

			@Override
			public void connectBack(boolean success) {
				blocking.put(key, success);
			}
		};
		NetworkModule nm = this.getModule(NetworkModule.class);
		if (nm.getConnectThread().isConnected()) {
			nm.closeConnect();
		}
		{
			// 因为负责连接服务器的和负责发送消息的线程不同，在执行登录之前是没有连接的，所以在这里先添加个连接后回掉的action
			// 当连接成功后再把登陆消息发出去，不然先把消息发了，再连接就没有执行登陆操作了
			nm.getConnectThread().addConnectBackAction(cba);
			nm.getConnectThread().setConnectData(connectData);
			nm.getConnectThread().setAutoConnect(true);
		}
		Boolean mark = blocking.get(key, 10, TimeUnit.SECONDS);
		if (mark == null) {
			mark = false;
		}
		return mark;
	}

	public PersonalRequester getPersonalRequester() {
		return this.getObject(PersonalRequester.class);
	}
}
