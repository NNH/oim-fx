package com.oim.demo;

import com.oim.core.api.OimContext;
import com.oim.core.api.bean.UserInfo;
import com.oim.core.api.data.ResultData;
import com.oim.core.api.requester.PersonalRequester;
import com.only.common.result.Info;
import com.only.common.result.util.MessageUtil;
import com.only.common.util.OnlyMD5Util;
import com.only.net.connect.ConnectData;
import com.onlyxiahui.im.bean.UserData;
import com.onlyxiahui.im.message.data.LoginData;

/**
 * @author: XiaHui
 * @date: 2018-03-24 16:49:05
 */
public class Demo {

	public static void main(String[] args) {
		// 第一步获取服务器地址；这里我不解析了，下面直接写死的
//		String url = "http://120.77.42.39:12000/api/v1/oim/server/getAddress.do";
//		Map<String, String> dataMap = new HashMap<String, String>();
//		String r = HttpUtil.post(url, dataMap);
//		System.out.println(r);

		ConnectData connectData = new ConnectData();
		connectData.setAddress("127.0.0.1");
		connectData.setPort(12010);

		OimContext oc = new OimContext();
		// 连接服务器
		boolean marck = oc.connect(connectData);

		if (marck) {

			LoginData loginData = new LoginData();
			loginData.setAccount("10001");
			loginData.setPassword(OnlyMD5Util.md5L32("123456"));
			loginData.setStatus("1");
			PersonalRequester pr = oc.getPersonalRequester();

			ResultData<UserInfo> rd = pr.login(loginData);

			if (rd.isSuccess()) {
				UserInfo ui = rd.get();
				UserData ud = ui.getUserData();
				System.out.println(ud.getNickname());
			} else {
				// 如果登录失败，info中有错误信息
				Info info = rd.getInfo();

				String error = MessageUtil.getDefaultErrorText(info);
				System.out.println(error);
				System.out.println("登录失败！");
			}
		} else {
			System.out.println("连接失败！");
		}
	}

}
