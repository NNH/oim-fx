package com.oim.demo;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author: XiaHui
 * @date: 2018-01-29 13:39:33
 */
public class HttpUtil {

	
	public static String post(String http,  Map<String, String> dataMap) {
		String result = "";
		HttpURLConnection conn = null;
		OutputStream out = null;
		try {
			URL url = new URL(http);
			conn = (HttpURLConnection) url.openConnection();
			conn.setConnectTimeout(5000);
			conn.setReadTimeout(30000);
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setUseCaches(false);
			
			conn.setInstanceFollowRedirects(true);  
			conn.setRequestMethod("POST"); // 设置请求方式  
			conn.setRequestProperty("Accept", "application/json"); // 设置接收数据的格式  
			conn.setRequestProperty("Content-Type", "application/json"); // 设置发送数据的格式  
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; zh-CN; rv:1.9.2.6)");
			
			conn.connect();  
			
			out = new DataOutputStream(conn.getOutputStream());
			// text
			putData(out, dataMap);
		
			// 读取返回数据
			StringBuffer sb = new StringBuffer();
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line).append("\n");
			}
			result = sb.toString();
			reader.close();
			reader = null;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				//conn.disconnect();
				conn = null;
			}
		}
		return result;
	}

	private static void putData(OutputStream out, Map<String, String> dataMap) {
		if (dataMap != null) {
			StringBuffer sb = new StringBuffer();
			Iterator<Map.Entry<String, String>> i = dataMap.entrySet().iterator();
			while (i.hasNext()) {
				Map.Entry<String, String> entry = i.next();
				String inputName = (String) entry.getKey();
				String inputValue = (String) entry.getValue();
				if (inputValue == null) {
					continue;
				}
				sb.append("Content-Disposition: form-data; name=\"" + inputName + "\"\r\n\r\n");
				sb.append(inputValue);
			}
			try {
				out.write(sb.toString().getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] arg){
		String url="http://120.77.42.39:12000/api/v1/oim/server/getAddress.do";
		Map<String, String> dataMap=new HashMap<String, String>();
		String r=post(url,dataMap);
		System.out.println(r);
	}
}
